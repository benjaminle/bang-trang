const express = require('express');
const fs = require('fs');
const app = express();
const bodyparser = require('body-parser');
const sha256 = require('js-sha256').sha256;
const mysql = require('mysql');
const url_api = require('url');
const nodemailer = require('nodemailer');
const randomWords = require('random-words');
const sqlString = require('sqlstring');
const api = "AIzaSyBJWHj5O83799DDa1atFa6Ayw16BDyakOg"
const google_translate = require('google-translate')(api);



const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'boardpoet08@gmail.com',
        pass: 'AnotherWordToFill'
    }
});

app.use(express.static('public'));
app.use(express.static('public/images'));
app.use(express.static('public/fontawesome'));
app.use(express.static('views/components'));

var change_password_list = [{code: 'a'}];
//Database code

var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '', //Correct password on server is 'AnotherWordToFill'
    database:'test'
})

var id = 0;

con.connect((err) => {
    if (err) {
        console.log("My SQL not connected");
    } else {
        let sql = "SELECT id FROM poems ORDER BY id DESC LIMIT 0, 1;"
        con.query(sql, function (err, results) {
            if (results[0] == undefined) {
                id = 0;
            } else {
                id = results[0]['id'];
            }
        });
        console.log("My SQL connection established");
    }
});

function insertPoem (poem_id, title, author, content, align) {
    if (con.state !== 'disconnected') {
        sql = "INSERT INTO poems (id, title, author, content, align) VALUES" +
            "(" + poem_id + "," + title + "," + author + ","+ content + "," + align+ ")";

        var flag = true;
        con.query(sql, (err) => {
            if (err) {
                flag = false;
            } else {
                flag = true;

            }
        });

        return flag;

    } else {

        return false;
    };
}


//App code

var user_hash = readUserHash("user-hash"); // is ordered by increasing lexographical order with user as index

function authenticate (uname, session_cookie) {
    var session_cookie_hash;

    if(session_cookie == undefined || session_cookie == "" || uname == undefined || uname == "") {
        return false;
    } else {
        session_cookie_hash = sha256(session_cookie);
    }

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);
    if (index == -1) {
        return false;
    } else {
        if ((user_hash[index].activated == "true" || user_hash[index].activated) && user_hash[index].onl_status == session_cookie_hash) {
            return true;
        } else {
            return false;
        }
    }
}

function readUserHash (datapath) {
    var array = [];

    let input = fs.createReadStream(datapath);

    var rl = require('readline').createInterface({
        input: input,
        terminal: false,
    });

    rl.on('line', function(line) {
        var arr = line.split(":");
        var usr = arr[0];
        var object = {
            usr: arr[0],
            email: arr[1],
            activated: arr[2],
            hash: arr[3],
            onl_status: arr[4]
        };
        if (array.length == 0) { // Makes sure is sorted
            array.push(object);
        } else {
            var i = 0;
            while (usr > array[i].usr) {
                i += 1;
                if (i == array.length) {
                    break;
                }
            }
            array.splice(i, 0, object);
        }
    });
    return array;
}

function saveUserHash () {
    var line = "";
    for (var i = 0; i < user_hash.length; i++) {
        line += user_hash[i].usr + ":" + user_hash[i].email + ":" + user_hash[i].activated + ":" + user_hash[i].hash + ":" + user_hash[i].onl_status +  "\n";
    }
    fs.writeFileSync("user-hash", line);
}

function binarySearch (array, l, r, object) {
    if (r >= l) {
        var mid = Math.floor(l + (r - l) / 2);

        // If the element is present at the middle
        // itself
        if (array[mid].usr == object)
            return mid;

        // If element is smaller than mid, then
        // it can only be present in left subarray
        if (array[mid].usr > object)
            return binarySearch(array, l, mid - 1, object);

        // Else the element can only be present
        // in right subarray
        return binarySearch(array, mid + 1, r, object);
    }

    // We reach here when element is not
    // present in array
    return -1;

}

function parseCookies (request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/authenticate', (req, res) => {
    var uname = req.query.uname;
    var session_cookie_hash = sha256(req.query.session_cookie);

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);

    if (index == -1) {
        res.send(false);
    } else {
        if ((user_hash[index].activated == "true" || user_hash[index].activated) && user_hash[index].onl_status == session_cookie_hash) {
            res.send(true);
        } else {
            res.send(false);
        }
    }
});

app.get('/test', (req, res) => {
    res.sendFile(__dirname + '/views/test.html');
});

app.post('/change_password', (req, res) => {
    var uname = req.body.uname;
    var current_pass = req.body.current_pass;
    var new_pass = req.body.new_pass;

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);
    if (index == -1) {
        res.send(false);
    } else {
        var c_pass = sha256(current_pass);
        if (c_pass == user_hash[index].hash) {
            user_hash[index].hash = sha256(new_pass);
            res.send(true);
        } else {
            res.send(false);
        }
    }
});

app.post('/blind_change_password', (req, res) => {
    var email = req.body.email;
    var new_pass = req.body.new_pass;



    var index = 0;
    var x = user_hash.length;

    var i = 0;
    while (i < user_hash.length) {
        if (user_hash[i].email == email) {
            index = i;
            break
        } else {
            i++;
        }
    }

    if (i < x) {
        user_hash[i].hash = sha256(new_pass);
        res.send(true);
    } else {
        res.send(false);
    }

});

app.get('/create_poem', (req, res) => {
    res.sendFile(__dirname + "/views/create_poem.html");
});

app.get('/quen-mat-khau', (req, res) => {
    res.sendFile(__dirname + '/views/forget-password/forget_password.html');
});

app.get('/get_front_page_new_poems', (req, res) => {
    var sql = "SELECT * FROM poems ORDER BY id DESC LIMIT 10;"
    con.query(sql, (err, results) => {
        if(err) {
            throw err;
        } else {
            if (results == undefined || results.length == 0) {
                res.send(JSON.stringify('null'));
            } else {
                res.send(JSON.stringify(results));
            }

        }
    })
});

app.get('/get_front_page_fire_poems', (req, res) => {
    var sql = "SELECT * FROM poems HAVING like_cnt + exc_cnt + insp_cnt + tear_cnt + fear_cnt + awe_cnt + sad_cnt > 1 ORDER BY id DESC LIMIT 10;"
    con.query(sql, (err, results) => {
        if(err) {
            throw err;
        } else {
            if (results == undefined || results.length == 0) {
                res.send(JSON.stringify('null'));
            } else {
                res.send(JSON.stringify(results));
            }

        }
    })
});

app.get('/trang-chu', (req, res) => {
    var list = parseCookies(req);
    var uname = decodeURIComponent(list.uname);
    var session_cookie = list.session_cookie;

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);
    if (index == -1) {
        res.redirect('/');
    } else {
        if (user_hash[index].onl_status == sha256(session_cookie)) {
            res.sendFile(__dirname + '/views/loggedin_change_password.html');
        } else {
            res.redirect('/');
        }
    }
});

app.get('/get_next_new_batch', (req,res) => {
    var index = req.query.index;

    var sql = "SELECT * FROM poems HAVING id < " + index + " ORDER BY id DESC LIMIT 10";

    con.query(sql, function (err, results) {
        if (err) {
            throw err;
        } else {
            if (results == undefined || results.length == 0) {
                res.send(JSON.stringify(null));
            } else {
                res.send(JSON.stringify(results));
            }
        }

    });
});

app.get('/get_next_fire_batch', (req,res) => {
    var index = req.query.index;

    var sql = "SELECT * FROM poems HAVING id < " + index + " like_cnt + exc_cnt + insp_cnt + tear_cnt + fear_cnt + awe_cnt + sad_cnt > 1 ORDER BY id DESC LIMIT 10;";

    con.query(sql, function (err, results) {
        if (err) {
            throw err;
        } else {
            if (results == undefined || results.length == 0) {
                res.send(JSON.stringify(null));
            } else {
                res.send(JSON.stringify(results));
            }
        }

    });
});

app.get('/get_next_noti_batch', (req, res) => {
    var uname = "\'" + req.query.uname + "\'";
    var offset = req.query.cnt;


    var next_noti_sql = "SELECT react.reactor, react.to_poem_id, poems.title, react.reaction, react.react_time " +
        "FROM react " +
        "INNER JOIN poems ON react.to_poem_id = poems.id WHERE poems.author = " + uname +
        "ORDER BY react_time DESC LIMIT 10 OFFSET " + offset;
    con.query(next_noti_sql, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify(results));
    })
});

app.get('/dang-nhap', (req, res) => {
    var list = parseCookies(req);

    var uname = decodeURIComponent(list.uname);
    if (list.session_cookie == undefined || list.uname == undefined) {
        res.sendFile(__dirname + '/views/login_page.html');
    } else {
        var session_cookie_hash = sha256(list.session_cookie);

        var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);
        if (index == -1) {
            res.sendFile(__dirname + '/views/login_page.html');
        } else {
            if ((user_hash[index].activated == "true" || user_hash[index].activated) && user_hash[index].onl_status == session_cookie_hash) {
                res.redirect('/');
            } else {
                res.sendFile(__dirname + '/views/login_page.html');
            }
        }
    }

});

app.post('/login_attempt', (req,res)  => {
    var psw = req.body.psw;
    var user = req.body.uname;

    var index = binarySearch(user_hash, 0, user_hash.length - 1, user);

    if (index >= 0) {
        if (user_hash[index].activated != "true" && user_hash[index].activated != true) {

            let activation_code = "";

            for (var i = 0; i < 7; i++) {
                activation_code += String.fromCharCode(Math.floor(Math.random() * 25) + 97);
            }

            user_hash[index].activated = activation_code;
            let href = 'phuong-do.com/' + user + '/' + activation_code + '/activate'


            let mailOptions = {
                from: 'boardpoet08@gmail.com',
                to: user_hash[index].email,
                subject: 'Your account activation code',
                html:'<p style="line-height:1em; color:black">' +
                    'Hello, the link to activate your account is: <br><br>' +
                    '<a href=' + href + '>phuong-do.com/' + user + '/' + activation_code + '/activate</a><br><br>' +
                    'Please click on this to activate your account' +
                    '</p>'
            };

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

            res.send(JSON.stringify('Your account hasn\'t been activated. A new activation link is sent.'));

        } else {
            console.log('login');

            var hash = sha256(psw);
            if (hash == user_hash[index].hash) {
                let session_cookie = "";
                for (let i = 0; i < 40; i++) {
                    session_cookie += String.fromCharCode(Math.floor(Math.random() * 25) + 97);
                }
                session_cookie_hash = sha256(session_cookie);
                user_hash[index].onl_status = session_cookie_hash;
                res.cookie('email', encodeURIComponent(user_hash[index].email), {maxAge: 1000 * 60 * 60 * 24 * 7}); // 1 week
                res.cookie('session_cookie', session_cookie, {maxAge: 1000 * 60 * 60 * 24 * 7});
                res.cookie('uname', encodeURIComponent(user), {maxAge: 1000 * 60 * 60 * 24 * 7});

                user_hash[index].onl_status = session_cookie_hash;
                res.send(JSON.stringify('success'));

            } else {
                res.send(JSON.stringify('incorrect password'));
            }
        }
    } else {
        res.send(JSON.stringify('Username is not in our databse'));
    }
});

app.get('/logout', (req,res) => {
    var list = parseCookies(req);
    var uname = decodeURIComponent(list.uname);
    var session_cookie = list.session_cookie;

    var index = binarySearch(user_hash, 0, user_hash.length -1, uname);

    var sha = sha256(session_cookie);
    if (sha == user_hash[index].onl_status) {

        user_hash[index].onl_status = "null";
    }

    res.cookie('uname', "");
    res.cookie('session_cookie',"");
    res.redirect('/');
});

app.get('/doc-tho', (req, res) => {
    res.sendFile(__dirname + '/views/poem.html');
});

app.get('/tac-gia', (req, res) => {
    res.sendFile(__dirname + '/views/poet.html');
});

app.get('/get_poet_poems', (req, res) => {

    var name = '\'' + req.query.poet_name + '\'';
    if (name == undefined) {
        res.send(JSON.stringify('null'));
    }

    var sql = "SELECT * FROM poems HAVING author = " + name + " ORDER BY id DESC LIMIT 10";
    con.query(sql, (err, results) => {
        if (err) throw err;
        if (results == undefined || results.length == 0) {
            res.send(JSON.stringify('null'));
        } else {
            res.send(JSON.stringify(results));
        }
    })
});

app.get('/get_next_poet_batch', (req, res) => {
    var poet_name = '\'' + req.query.poet_name + '\'';
    var index = req.query.index;

    var sql = "SELECT * FROM poems HAVING id < " + index + " AND author = " + poet_name + "ORDER BY id DESC LIMIT 10;";

    con.query(sql, function (err, results) {
        if (err) throw err;
        if (results == undefined || results.length == 0) {
            res.send(JSON.stringify(null));
        } else {
            res.send(JSON.stringify(results));
        }


    });
})

app.post('/authenticate', (req, res) => {
    var uname = req.body.uname;
    var session_cookie = req.body.session_cookie;
    var session_cookie_hash;

    if(session_cookie == undefined || session_cookie == "" || uname == undefined || uname == "") {
        res.send(false);
    } else {
        session_cookie_hash = sha256(req.body.session_cookie);
    }

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);
    if (index == -1) {
        res.send(false);
    } else {
        if ((user_hash[index].activated == "true" || user_hash[index].activated) && user_hash[index].onl_status == session_cookie_hash) {
            res.send(true);
        } else {
            res.send(false);
        }
    }


})

app.get('/doi-mat-khau', (req, res) => {
    var list = parseCookies(req);
    var uname = decodeURIComponent(list.uname);
    var session_cookie = list.session_cookie;
    if (uname == undefined || uname == null || session_cookie == undefined || session_cookie == null) {
        res.redirect('/dang-nhap');
    } else {
        var session_cookie_hash = sha256(session_cookie);
        var index = binarySearch(user_hash, 0, user_hash.length-1, uname);

        if (index == -1 || user_hash[index].onl_status != session_cookie_hash) {
            res.redirect('/dang-nhap');
        } else {
            res.sendFile(__dirname + '/views/loggedin_change_password.html');
        }
    }
});

app.post('/submit_poem', (req, res) => {
    var author = req.body.author;
    var session_cookie = req.body.session_cookie;

    if (authenticate(author, session_cookie)) {

        var title = req.body.title;
        var content = req.body.content;
        var align = req.body.align;

        author = author.replace(/%20/g, " ");

        // var poem_id = "";
        id += 1;
        if (title == "" || title == undefined) {
            title = "Un-named";
        }
        author = sqlString.escape(author);
        title = sqlString.escape(title);
        content = content.replace(/\n/g, "<br>");
        content = sqlString.escape(content);
        align = sqlString.escape(align);


        if (insertPoem(id, title, author, content, align)) {
            res.send(JSON.stringify('Bài thơ của bạn đã được đăng. Hãy đợi 3 giây trước khi bạn đăng bài mới.'));
        } else {
            res.send(JSON.stringify('Xin lỗi, chúng tôi không thể đăng bài thơ của bạn'));
        }
    } else {
        res.send(JSON.stringify('Xin lỗi, chúng tôi không thể đăng bài thơ của bạn'));
    }
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

app.get('/kich-hoat-tai-khoan', (req, res) => {
    var list = parseCookies(req);
    var activation_email = decodeURIComponent(list.activation_email);

    var valid_email = false;
    var index = 0;
    for (var i = 0; i < user_hash.length; i++) {

        if (user_hash[i].email == activation_email) {
            valid_email = true;
            index = i;
            break;
        }
    }

    if (valid_email) res.sendFile(__dirname + '/views/resend_activation_email.html');
    else res.redirect('/tao-tai-khoan');
})

app.get('/resend_activation_email', (req, res) => {
    var list = parseCookies(req);
    var activation_email = decodeURIComponent(list.activation_email);

    var valid_email = false;
    var index = 0;
    for (var i = 0; i < user_hash.length; i++) {

        if (user_hash[i].email == activation_email) {
            valid_email = true;
            index = i;
            break;
        }
    }


    if (valid_email) {
        var uname = user_hash[index].usr;
        var activation_code = user_hash[index].activated;

        let href = 'phuong-do.com/'+ uname + '/' + activation_code + '/activate';

        let mailOptions = {
            from: 'boardpoet08@gmail.com',
            to: activation_email,
            subject: 'Your account activation code',
            html:'<p style="line-height:1em; color:black">' +
                'Hello, the link to change your password is: <br><br>' +
                '<a href=' + href + '>phuong-do.com/'+ uname + '/' + activation_code + '/activate</a><br><br>' +
                'A window will open activate your account' +
                '</p>'
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        res.send(true);

    } else {
        res.send(false);
    }

})
app.post('/sign_up', (req, res) => {
    var uname = req.body.uname;
    var psw = req.body.psw;
    var email = req.body.email;

    var index = binarySearch(user_hash, 0, user_hash.length - 1, uname);

    if (!validateEmail(email)) { //has to check twice since JS client side can be turned off
        res.send(JSON.stringify("Email không hợp lệ"));
    } else if (index > -1) { //if account exists
        res.send(JSON.stringify("Tên này đã được dùng"));
    } else { // else
        var email_used = false;
        var j= 0;
        while(j < user_hash.length) {
            if (user_hash[j].email == email) {
                email_used = true;
                break;
            }
            j++;
        }

        if(email_used) {
            res.send(JSON.stringify('Email đã được dùng'));
        } else {
            let h = sha256(psw);

            let activation_code = "";

            for (var i = 0; i < 7; i++) {
                activation_code += String.fromCharCode(Math.floor(Math.random() * 25) + 97);
            }

            var object = {
                usr: uname,
                hash: h,
                activated: activation_code,
                email: email,
                user_hash: "null",
                onl_status: false
            };

            let href = 'phuong-do.com/'+ uname + '/' + activation_code + '/activate';

            let mailOptions = {
                from: 'boardpoet08@gmail.com',
                to: email,
                subject: 'Your account activation code',
                html:'<p style="line-height:1em; color:black">' +
                    'Hello, the link to activate your account is: <br><br>' +
                    '<a href=' + href + '>phuong-do.com/'+ uname + '/' + activation_code + '/activate</a><br><br>' +
                    'A window will open to activate your account' +
                    '</p>'
            };

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });


            if (user_hash.length == 0) {
                user_hash.push(object);
            } else {
                var i = 0;
                while (i < user_hash.length && uname > user_hash[i].usr) { //insert new user into list in increasing order
                    if (i == user_hash.length) {
                        break;
                    } else {
                        i+= 1;
                    }

                }
                user_hash.splice(i, 0, object);

            }
            res.send(JSON.stringify("Thành công!"));
        }
    }
});

app.get('/tao-tai-khoan', (req, res) => {
    res.sendFile(__dirname + '/views/sign-up.html');
})

app.get('/saveUserHash', (req, res) => {
    saveUserHash();
    res.send(true);
});

app.get('/get-noti', (req,res) => {
    var uname = "\'" + req.query.uname + "\'";

    var get_noti_sql = "SELECT react.reactor, react.to_poem_id, poems.title, react.reaction, react.react_time " +
        "FROM react " +
        "INNER JOIN poems ON react.to_poem_id = poems.id WHERE poems.author = " + uname +
        "ORDER BY react_time DESC LIMIT 10";
    con.query(get_noti_sql, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify(results));
    })
});

app.post('/send_email_to_change_password', (req, res) => {
    var email = req.body.email;
    var contain_email = false;

    for (var i = 0 ; i < user_hash.length; i++) {

        if (user_hash[i].email == email) {
            contain_email = true;
            break;
        }
    }

    if (contain_email) {
        let x = "";
        for (let i = 0 ; i < 20; i++) {
            x += String.fromCharCode(Math.floor(Math.random() * 25) + 97);
        }
        change_password_list.push({code: x, email: email, exp: Date.now() + 1000*60*5});
        let href = "phuong-do.com/change-password/" + x;



        let mailOptions = {
            from: 'boardpoet08@gmail.com',
            to: email,
            subject: 'Reset your password',
            html:'<p style="line-height:1em; color:black">' +
                'Hello, the link to change your password is: <br><br>' +
                '<a href=' + href + '>phuong-do.com/change-password/' + x + '</a><br><br>' +
                'A window will open to change your password.' +
                '</p>'
        };
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
        res.send(true);
    } else {
        res.send(false);
    }

});

var updatePasswordList = function () {
    var i= 0;
    var now = new Date();
    now.setHours(0,0,0,0);
    while (i < change_password_list.length) {
        if (change_password_list[i].exp < now) {
            change_password_list.splice(i, 1);
        } else {
            i++;
        }
    }
    console.log('updated password list');
};

updatePasswordList();
setInterval(updatePasswordList, 1000* 60* 5); // clear outdate password list every 5 minutes - ish

var dir_length;
var daily_inspiration_obj = {};
var current_insp_index = 0;


app.get('/get-inspiration-of-the-day', (req, res) => {
    res.send(JSON.stringify(daily_inspiration_obj));
});

app.post('/react', (req, res) => {
    var reactor = req.body.uname;
    var poem_id = req.body.poem_id;
    var reaction = req.body.reaction;
    var time = req.body.time;
    reactor = "\"" + reactor + "\"";


    var reaction_time = new Date(time).toISOString();


    if (reaction == "excited") {
        reaction = "exc";
    } else if (reaction == "inspired") {
        reaction = "insp";
    } else if (reaction == "tearful") {
        reaction = "tear";
    } else if (reaction == "fearful") {
        reaction = "fear";
    }

    var update_clm_in_poems = reaction + "_cnt";

    reaction = "\'" + reaction + "\'";



    var sql = "SELECT COUNT(1) FROM react WHERE reactor = " + reactor + " AND to_poem_id = " + poem_id;

    con.query(sql, function (err, results) {
        if (err) throw err;

        if (results[0]['COUNT(1)'] == 0) {

            var insert_reaction_sql = "INSERT INTO react (reactor, to_poem_id, reaction, react_time) VALUES (" +
                    reactor + ", " + poem_id + ", " + reaction + ", \'" + reaction_time + "\')";
            con.query(insert_reaction_sql);

            var increment_reaction_cnt  = "UPDATE poems SET " + update_clm_in_poems + " = " + update_clm_in_poems + " + 1 WHERE id = " + poem_id;

            con.query(increment_reaction_cnt);


        } else if (results[0]['COUNT(1)'] == 1) {


            var get_previous_reaction = "SELECT reaction FROM react WHERE reactor = " + reactor + "AND to_poem_id = " + poem_id;

            con.query(get_previous_reaction, (err, results) => {
                if (err) throw err;

                var previous_reaction_cnt = results[0].reaction + "_cnt";


                var update_sql = "UPDATE react SET reaction = "+ reaction + ", react_time = \'" + reaction_time + "\' WHERE reactor = " + reactor + " AND to_poem_id = " + poem_id;
                con.query(update_sql);


                var increment_new_reaction_cnt_sql = "UPDATE poems SET " + update_clm_in_poems + " = " + update_clm_in_poems + " + 1 WHERE id = " + poem_id;


                con.query(increment_new_reaction_cnt_sql, (err, results) => {
                    if (err) throw err;
                    var decrement_old_reaction_cnt_sql = "UPDATE poems SET " + previous_reaction_cnt + " = " + previous_reaction_cnt + " - 1 WHERE id = " + poem_id;
                    con.query(decrement_old_reaction_cnt_sql);
                })

            });

        }

    })


});

newInspiration()
setInterval(newInspiration, 1000*60*60*1);

function newInspiration() {
    var inspiration_image_name = "";

    fs.readdir(__dirname + '/public/images', function(err, items) {
        items.shift();

        dir_length = items.length;
        if (current_insp_index >= items.length) {
            current_insp_index = 0;
        }

        inspiration_image_name = items[current_insp_index];
        var random_words = randomWords(5 );

        var translated_words = [];

        for (var i = 0; i < random_words.length; i++) {
            google_translate.translate(random_words[i], 'vi', function(err, translation) {
                translated_words.push(translation.translatedText);
            });
        }

        setTimeout(proceed, 1000*4);

        function proceed() {
            var random_word_string = translated_words[0] + ', ' + translated_words[1] + ', ' + translated_words[2] + ', ' + translated_words[3] + ', ' + translated_words[4];

            daily_inspiration_obj = {
                src: inspiration_image_name,
                word: random_word_string
            }
            current_insp_index += 1;
        }


    })
}

app.get('/*', (req,res) => {
    var url = url_api.parse(req.url, true);
    var pathname = url.pathname;
    var cnt = 0;
    var slash_loc = [];
    for (var i = 0; i < pathname.length; i++) {
        if (pathname.charAt(i) == "/") {
            slash_loc.push(i);
            cnt += 1;
        }
    }
    if (cnt == 3) {
        let activate;
        let uname = "";
        let activate_code = "";

        activate = pathname.substring(slash_loc[2]+1);

        if (activate != "activate") {
            res.send('incorrect url');
        } else {
            uname = pathname.substring(slash_loc[0] + 1, slash_loc[1]);
            activate_code = pathname.substring(slash_loc[1]+1, slash_loc[2]);
            let index = binarySearch(user_hash, 0, user_hash.length -1, uname);
            if (user_hash[index].activated == activate_code) {
                user_hash[index].activated = true;
                res.send('account activated');
            } else {
                res.send('incorrect activation code');
            }

        }

    } else if (cnt ==2) {

        //action_code first = ac
        var action_code = pathname.substring(slash_loc[0] + 1, slash_loc[1]);
        var code = pathname.substring(slash_loc[1]+1);

        if (action_code == "change-password") {
            // href = /change-password/code

            var i = 0;
            var code_is_in = false;
            var email = "";
            while (i < change_password_list.length) {
                if (change_password_list[i].code == code) {
                    email = change_password_list[i].email;
                    code_is_in = true;
                    break;
                } else {
                    i++;
                }
            };
            if (code_is_in) {

                res.cookie('email', encodeURIComponent(email));
                res.sendFile(__dirname + '/views/forget-password/change_password.html');
            } else if (code == "pure-navbar.html" || code == "navbar.html"){
                res.sendFile(__dirname + "/views/components/" + code); // should be html page
            } else {
                res.send('invalid_url');
            }
        } else if (action_code == "get-poem") {
            // href = /get-poem/code
            var sql = "SELECT * FROM poems WHERE id = " + code;
            var obj;

            con.query(sql, function (err, results) {
                if (err) {
                    throw err;
                } else {
                    if (results == undefined || results.length == 0) {
                        res.send(JSON.stringify('null'));
                    } else {
                        res.send(JSON.stringify(results[0]));
                    }
                }

            });
        }

    } else  {
        res.send("Error, directory not found");
    }
});


setInterval(saveUserHash,1000* 60* 5) //save users everyday;

const port = process.env.PORT || 3001;

app.listen(port,() => console.log(`listening on port ${port}`));